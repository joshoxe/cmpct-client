const { app, BrowserWindow, ipcMain } = require("electron");
const Client = require("./src/Client.js");

let channelWindow;
let client;
let user;

async function createWindow() {
  channelWindow = new BrowserWindow({
    width: 1920,
    height: 1080,
    show: false,
    backgroundColor: "#fff",
    webPreferences: {
      nodeIntegration: true
    }
  });

  // The loading window needs to appear first while the IRC connection and main window is established
  loadingWindow = new BrowserWindow({
    width: 810,
    height: 610,
    frame: false,
    alwaysOnTop: true,
    backgroundColor: "#fff"
  });
  await loadingWindow.loadFile("./public/loading.html");

  client = new Client(channelWindow, loadingWindow);
  client.run();
}

app.on("ready", createWindow);

app.on("window-all-closed", () => {
  channelWindow.webContents.send("user-quit");
  client.quit("Client closed");
  if (process.platform !== "darwin") app.quit();
});

app.on("activate", () => {
  if (mainWindow === null) createWindow();
});

/**
 * Received when the user has sent a message to the server
 */
ipcMain.on("message", (event, data) => {
  client.say(data.channel, data.message);
});

/**
 * Received when the user quits the client
 */
ipcMain.on("current-channel-save", (event, data) => {
  // Save the last viewed channel when the user quits
  if (user === null) {
    return;
  }

  user.saveCurrentChannel(data);
});

/**
 * Received when the user clicks to join a new channel
 */
ipcMain.on("join-channel", (event, data) => {
  client.joinChannel(data);
});

ipcMain.on("request-user-list", (event, data) => {
  const channel = client.getChannelByName(data);
  const namesInfo = {
    channel: data,
    users: channel.channel.users
  };
  channelWindow.webContents.send("names", namesInfo);
});

ipcMain.handle("get-topic", async (event, channelName) => {
  const topic = await client.getChannelByName(channelName).getTopic();
  return topic;
});
