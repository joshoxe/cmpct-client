/** Class representing a channel within the client */
class Channel {
  /**
   * Create a channel
   * @param {string} name the channel name
   * @param {irc-framework.Client} framework a connected instance of irc-framework
   * @param {BrowserWindow} window a reference to the main window
   */
  constructor(name, framework, window) {
    this.channel = framework.channel(name);
    this.window = window;
    this.name = name;
    this.users = [];
    this.topic = "";
  }

  async loadChannel() {
    this.channel.join();
    this.topic = this.channel.topic;
    this.reloadUserList(this.channel.users);
    return true;
  }

  /**
   * Return the irc-framework channel object
   */
  getChannel() {
    return this.channel;
  }

  /**
   * Update the user list and send this to the main window
   * @param {array} users a list of new users in the channel
   */
  reloadUserList(users) {
    this.users = [];
    users.forEach(user => {
      const name = user.nick;
      this.users.push(name);
    });
  }

  setTopic(topic) {
    this.topic = topic;
  }

  getTopic() {
    return this.topic;
  }
}

module.exports = Channel;
