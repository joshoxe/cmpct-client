const Listener = require("./Listener.js");

/**
 * Handlers for events regarding the registration process, i.e. connecting to the server and initially joining channels.
 */

class Registration extends Listener {
  /**
   * Create a new registration listener
   * @param {irc-framework.Client} framework a connected instance of irc-framework
   * @param {BrowserWindow} window a reference to the main window
   * @param {Client} chatClient the Client object
   */
  constructor(framework, window, chatClient) {
    super(framework, window, chatClient);
    this.handlers = {};
    this.RegisterHandlers();
  }

  /**
   * Register the functions for each handler -
   * must be added to this.handlers as a command:function pair
   */
  RegisterHandlers() {
    this.handlers["registered"] = this.RegistrationHandler;
    this.handlers["channel list"] = this.ListHandler;
  }

  /**
   * Handler for when the client has connected to the server. Requests a channel list from `client` and joins any previous channels
   * @param {object} event the irc-framework event object
   * @param {object} client the irc-framework client object
   * @param {Channels} self a self-reference to access class properties
   */
  RegistrationHandler(event, client, self) {
    // Request a channel list
    client.list();
  }

  /**
   * Handler for `channel list` which lists every channel in the server.
   * `event` contains an array of all channel objects returned
   * @param {object} event the irc-framework event object
   * @param {object} client the irc-framework client object
   * @param {Channels} self a self-reference to access class properties
   */
  ListHandler(event, client, self) {
    // Event contains every public channel on the server
    const channels = event;

    self.chatClient.loadAllChannels(channels);
    self.chatClient.register();
  }
}

module.exports = Registration;
