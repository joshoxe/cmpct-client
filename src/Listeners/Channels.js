const Listener = require("./Listener.js");

/**
 * Handlers for all events regarding channel activity, i.e. messaging, user lists & topics.
 */

class Channels extends Listener {
  /**
   * Create a new channel listener
   * @param {irc-framework.Client} framework a connected instance of irc-framework
   * @param {BrowserWindow} window a reference to the main window
   * @param {Client} chatClient the Client object
   */
  constructor(framework, window, chatClient) {
    super(framework, window, chatClient);
    this.handlers = {};
    this.RegisterHandlers();
  }

  /**
   * Register the functions for each handler -
   * must be added to this.handlers as a command:function pair
   */
  RegisterHandlers() {
    this.handlers["message"] = this.MessageHandler;
    this.handlers["userlist"] = this.UserListHandler;
    this.handlers["topic"] = this.TopicHandler;
  }

  /**
   * Handle incoming messages
   * @param {object} event the irc-framework event object
   * @param {object} client the irc-framework client object
   * @param {Channels} self a self-reference to access class properties
   */
  MessageHandler(event, client, self) {
    // Could be sent to client rather than to GUI here - could also store all messages (`event`s) received in Client?
    if (self.framework.connected) {
      self.window.webContents.send("message", {
        from: event.nick,
        to: event.target,
        message: event.message
      });
    }
  }

  /**
   * Handle a `userlist` event which refreshes a channels user list.
   * `event` contains a channel name and array of users
   * @param {object} event the irc-framework event object
   * @param {object} client the irc-framework client object
   * @param {Channels} self a self-reference to access class properties
   */
  UserListHandler(event, client, self) {
    if (self.framework.connected) {
      const channel = self.chatClient.getChannelByName(event.channel);

      channel.reloadUserList(event.users);
      self.window.webContents.send("names", {
        channel: channel.channel.name,
        users: event.users
      });
    }
  }

  TopicHandler(event, client, self) {
    if (self.framework.connected) {
      const channel = self.chatClient.getChannelByName(event.channel);
      channel.setTopic(event.topic);
      self.chatClient.changeTopic(channel);
    }
  }
}

module.exports = Channels;
