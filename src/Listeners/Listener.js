/**
 * Super class for all listeners in the client
 *
 * A new `Listener` should never happen, this is mostly an abstract class.
 */

class Listener {
  /**
   * @param {irc-framework.Client} framework a connected instance of irc-framework
   * @param {BrowserWindow} window a reference to the main window
   * @param {Client} chatClient the Client object
   */
  constructor(framework, window, chatClient) {
    this.framework = framework;
    this.window = window;
    this.chatClient = chatClient;
    this.handlers = {};
  }

  RegisterHandlers() {}

  /**
   * Called whenever the irc-framework receives a parsed event.
   *
   * Every subclass of `Listener` keeps a command:function list of handlers and this function will search through every registered
   * handler in these subclasses and execute the *first* one it finds which matches `command`.
   * @param {string} command string description of the event
   * @param {object} event event object containing any related data returned in the event
   * @param {object} client the irc-framework client object
   */
  handleEvent(command, event, client) {
    if (!(command in this.handlers)) {
      return;
    }

    this.handlers[command](event, client, this);
  }
}

module.exports = Listener;
