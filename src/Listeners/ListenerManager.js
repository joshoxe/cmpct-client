const ChannelListeners = require("./Channels.js");
const RegistrationListeners = require("./Registration.js");

class ListenerManager {
  constructor(framework, window, client) {
    this.framework = framework;
    this.window = window;
    this.listeners = [];
    this.listeners.push(new ChannelListeners(framework, window, client));
    this.listeners.push(new RegistrationListeners(framework, window, client));
  }

  send(command, event, client) {
    this.listeners.forEach(listener => {
      listener.handleEvent(command, event, client);
    });
  }
}

module.exports = ListenerManager;
