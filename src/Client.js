const irc = require("irc-framework");
const User = require("./User.js");
const Channel = require("./Channel.js");
const ListenerMan = require("./Listeners/ListenerManager.js");

let framework;

/** Main class representing the client itself  */
class Client {
  /**
   * Create a client object
   * @param {BrowserWindow} window A reference to the main window
   */
  constructor(window, loadingWindow) {
    this.window = window;
    this.loadingWindow = loadingWindow;
    this.user = new User("client");
    this.channels = [];
    this.activeChannel = {};
    this.servers = {};
  }
  /**
   * Start the client, connect to the IRC server and start handling events
   */
  run() {
    framework = new irc.Client({
      nick: "lorem",
      username: "lorem",
      host: "localhost",
      port: 6667
    });
    framework.connect();

    // Tell the framework to use createListenerManager to manage events
    framework.use(this.createListenerManager(this));

    return true;
  }

  async register() {
    const channels = await this.getChannels("cmpct");
    await this.window.loadFile("./public/channel.html");
    await this.joinChannels();

    this.window.webContents.send("user-info", this.user.getUsername());
    this.window.webContents.send("servers", this.user.getServers());
    this.window.webContents.send("all-channels", channels);

    this.switchChannel(this.user.getLastViewedChannel("cmpct"));
    this.loadingWindow.destroy();
    this.window.show();
  }

  /**
   * Creates the ListenerManager object, then returns a function for framework.use()
   * which tells the framework to use sendEvents() for any parsed events received.
   * @param {Client} self The client object, should be passed as this
   */
  createListenerManager(self) {
    const ListenerManager = new ListenerMan(framework, self.window, self);
    return function(client, raw_events, parsed_events) {
      parsed_events.use(sendEvents);
    };

    /**
     * Fired when the framework receives a parsed event
     * @param {string} command String IRC command describing the event
     * @param {object} event The event object carrying any related data for the event
     * @param {object} client The irc-framework client object
     */
    function sendEvents(command, event, client, next) {
      // Sends the received event to the Listener Manager in order to delegate the event
      ListenerManager.send(command, event, client);
      next();
    }
  }

  /**
   * Return the IRC user object
   */
  getUser() {
    return this.user;
  }

  loadAllChannels(channels) {
    // Hard coding to cmpct for now, will need to have multiple server:channels pairs
    this.servers["cmpct"] = channels;
  }

  /**
   * Get all channels from a server
   * @param {string} server server name
   */
  getChannels(server) {
    return this.servers[server];
  }

  /**
   * Changes the active (focused) channel and sends this information to the main window
   * @param {string} channelName Name of channel to switch to
   */
  switchChannel(channelName) {
    const channel = this.getChannelByName(channelName);
    const channelObject = channel.getChannel();
    this.activeChannel = channel;
    channel.reloadUserList(channelObject.users);
    this.window.webContents.send("switch-channel", channelObject.name);
  }

  /**
   * Join all initial channels stored in the user's connection object
   */
  async joinChannels() {
    const channels = this.user.getChannels();
    channels.forEach(channelName => {
      const channel = new Channel(channelName, framework, this.window);
      channel.loadChannel();
      this.channels.push(channel);
      this.window.webContents.send("user-join-channel", channel.channel.name);
    });

    return this.channels;
  }

  joinChannel(channelName) {
    const channel = new Channel(channelName, framework, this.window);
    this.channels.push(channel);
    this.window.webContents.send("user-join-channel", channel.channel.name);
    const data = {
      channel: channel.name,
      users: channel.users
    };
    this.window.webContents.send("names", data);
  }

  /**
   * Retrieve a channel from the Client via its name
   * @param {string} name Name of channel
   */
  getChannelByName(name) {
    let chan = {};
    this.channels.forEach(channel => {
      if (channel.channel.name === name) {
        chan = channel;
      }
    });

    return chan;
  }

  getUser() {
    return this.user;
  }

  quit(reason) {
    framework.quit(reason);
  }

  say(channel, message) {
    framework.say(channel, message);
  }

  changeTopic(channel) {
    this.window.send("set-topic", {
      channel: channel.name,
      topic: channel.topic
    });
  }
}

module.exports = Client;
