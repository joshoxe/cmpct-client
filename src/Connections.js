const fs = require("fs");
const { app } = require("electron");
const ParameterNotFoundError = require("./exceptions/ParameterNotFoundError");

/**
 * Manage the clients connections - servers and channels in one object stored locally
 */

class Connections {
  /**
   * Create a new connection object
   */
  constructor() {
    const path = this.getFilePath();
    fs.access(path, error => {
      if (error) {
        this.saveConnections("");
      }
    });

    this.connections = JSON.parse(fs.readFileSync(path));
  }

  /**
   * Add a server to the connections file.
   * Must be an object that includes address & port, channels can also be added here.
   * @param {object} server
   */
  addServer(server) {
    if (typeof server !== "object") {
      throw new ParameterNotFoundError("Incorrect server object");
    }
    if (
      !Object.prototype.hasOwnProperty.call("server", server) ||
      !Object.prototype.hasOwnProperty.call("port", server)
    ) {
      throw new ParameterNotFoundError(
        "Server object does not include server or port"
      );
    }
    const newKey = this.getNewKey();
    this.connections[newKey].server = server.server;
    this.connections[newKey].port = server.port;
    this.saveConnections();
  }

  /**
   * Adds an array of channels to the server specified with an address
   * @param {string} serverAddress String
   * @param {array} channelsArray Array of string channels
   */
  addChannels(serverAddress, channelsArray) {
    if (typeof serverAddress !== "string" || !Array.isArray(channelsArray)) {
      throw new ParameterNotFoundError("Incorrect addChannels parameter");
    }

    Objects.keys(this.connections).forEach(server => {
      if (Object.prototype.hasOwnProperty.call("server", server)) {
        // Does this work??
        if (server.server === serverAddress) {
          if (Object.prototype.hasOwnProperty.call("channels", server)) {
            channelsArray.forEach(channel => {
              server.channels.push(channel);
            });
          } else {
            server.channels = channelsArray;
          }
        }
      }
    });
    this.saveConnections();
  }

  /**
   * Returns entire connections object
   */
  getConnections() {
    return this.connections;
  }

  /**
   * Returns the channels from a server
   * @param {string} server The server name
   */
  getChannels(serverName) {
    let channels = [];
    Object.keys(this.connections).forEach(server => {
      if (Object.prototype.hasOwnProperty.call("server", server)) {
        if (this.connections[server].server === serverName) {
          if (Object.prototype.hasOwnProperty.call("channels", server)) {
            channels = this.connections[server].channels;
          }
        }
      }
    });

    return channels;
  }

  /**
   * Counts the keys in the connections object and returns a new index (final index + 1)
   * Potentially slow, could be cached instead
   */
  getNewKey() {
    let count = 0;
    Object.keys(this.connections).forEach(keys, () => {
      count += 1;
    });
    return count.toString();
  }

  /**
   * Returns file path to OS appdata/cmpct-client/connections.json
   */
  getFilePath() {
    return app.getPath("appData") + "\\cmpct-client\\connections.json";
  }

  /**
   * Saves the connections file
   */
  saveConnections(data = this.connections) {
    const path = this.getFilePath();
    fs.writeFile(path, data, { flag: "w" }, error => {
      if (error) throw error;
    });
  }

  /**
   * Store the last viewed channel
   * @param {string} channelName The channel name
   * @param {string} serverName The server name
   */
  saveCurrentChannel(channelName, serverName) {
    Object.keys(this.connections).forEach(server => {
      if (Object.prototype.hasOwnProperty.call("server", server)) {
        if (this.connections[server].server === serverName) {
          this.connections[server].lastViewedChannel = channelName;
        }
      }
    });
  }

  /**
   * Get the channel the user was last focused on in the server
   * @param {string} serverName name of server
   */
  getLastViewedChannel(serverName) {
    let channel = "";
    Object.keys(this.connections).forEach(server => {
      if (Object.prototype.hasOwnProperty.call("server", server)) {
        if (this.connections[server].server === serverName) {
          if (
            Object.prototype.hasOwnProperty.call("lastViewedChannel", server)
          ) {
            channel = this.connections[server].lastViewedChannel;
          }
        }
      }
    });

    return channel;
  }
}

module.exports = Connections;
