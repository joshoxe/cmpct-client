const Connections = require("./Connections");

/**
 * Class representing the user, holds their server configurations and channels they are a part of
 */

class User {
  /**
   * Create a new user
   * @param {string} username nick to use on the client
   */
  constructor(username) {
    this.username = username;
    this.connections = new Connections();
    this.servers = this.connections.connections;
    // TODO: This channel storage works for now, but soon will need to be key'd by server
    // { server: [ channels ], ... }
    this.channels = [];
    this.createChannels();
  }

  /**
   * Create channels from the channel lists
   */
  createChannels() {
    const channelList = this.connections.getChannels("cmpct");
    channelList.forEach(channel => {
      this.channels.push(channel);
    });
  }

  /**
   * Return the channel list
   */
  getChannels() {
    return this.channels;
  }

  /**
   * Handle actions to perform when the client exits
   * @param {string} channel The current channel name
   */
  saveCurrentChannel(channel) {
    // Save the last viewed channel
    this.connections.saveCurrentChannel(channel);
  }

  /**
   * Get the channel which the user was focused on when they last connected to this server
   * @param {string} serverName name of server
   */
  getLastViewedChannel(serverName) {
    return this.connections.getLastViewedChannel(serverName);
  }

  /**
   * Return a channel object from its name
   * @param {string} channelName name of channel
   */
  getChannel(channelName) {
    let foundChannel = {};
    const channels = this.getChannels();
    channels.forEach(channel => {
      if (channel.name === channelName) {
        foundChannel = channel;
      }
    });

    return foundChannel;
  }

  getUsername() {
    return this.username;
  }

  getServers() {
    return this.connections;
  }
}

module.exports = User;
