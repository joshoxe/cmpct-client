/**
 * channel.js
 * All channel-specific events and logic. Handles incoming/outgoing messages, userlists and rendering for the channel screen.
 */

const electron = require("electron");
const { ipcRenderer } = electron;

// Global logic
let nick;
let currentChannel = "";

/**
 * Listener for 'user-info' event containing information from the user object
 */
ipcRenderer.on("user-info", (event, data) => {
  nick = data;
});

/**
 * Listener for the 'message' event which is sent when a message is received
 */
ipcRenderer.on("message", (event, data) => {
  displayMessage(data.to, data.from, data.message);
});

/**
 * Listener for the 'names' event which contains user list data
 */
ipcRenderer.on("names", (event, data) => {
  document
    .getElementById("user-list")
    .getElementsByClassName(data.channel + "_List")[0].innerHTML = "";

  data.users.forEach(name => {
    document
      .getElementById("user-list")
      .getElementsByClassName(data.channel + "_List")[0].innerHTML +=
      "<p>" + name.nick + "</p>";
  });
});

/**
 * Listener for the 'servers' event which contains server list data
 */
ipcRenderer.on("servers", (event, data) => {
  document.getElementById("server-list").innerHTML = "";
  document.getElementById("channel-list").innerHTML = "";
  Object.keys(data).forEach(server => {
    document.getElementById("server-list").innerHTML +=
      "<p>" + data[server][0].server + "</p>";
  });
});

/**
 * Listener for 'all-channels' which contains data for each channel within a server
 */
ipcRenderer.on("all-channels", (event, data) => {
  data.forEach(channel => {
    document.getElementById("channel-list").innerHTML +=
      '<button type="button" id="' +
      channel.channel.substring(1) +
      'ChannelList" class="list-group-item list-group-item-action list-group-item-secondary" onclick="switchChannel(\'' +
      channel.channel +
      "', this)\">" +
      channel.channel +
      "</button>";
  });
});

/**
 * Listener for 'user-quit' which is sent when the user closes the client
 */
ipcRenderer.on("user-quit", () => {
  ipcRenderer.send("current-channel-save", currentChannel);
});

/**
 * Listener for 'user-join-channel' which is sent when the user joins a new channel
 */
ipcRenderer.on("user-join-channel", (event, data) => {
  joinHandler(data);
});

/**
 * Listener for 'switch-channel' which is sent when a user clicks on a different channel in the channel list
 */
ipcRenderer.on("switch-channel", (event, data) => {
  // Need channel list element reference, channel list buttons ID'd by their name (data) minus the first character (#)
  const element = document.getElementById(data.substring(1) + "ChannelList");
  switchChannel(data, element);
});

ipcRenderer.on("set-topic", (event, data) => {
  // Only set topic in renderer if it's the active channel
  if (currentChannel !== data.channel) {
    return;
  }

  document.getElementById("channel-topic").innerHTML = data.topic;
});

// Get key presses from input box
const input = document.getElementById("input");

document.addEventListener("keypress", sendMessage);

/**
 * Function for sending a message to the server,
 * listens for the enter key being pressed and sends the text from the message input box
 * @param {event} e The event containing key data
 */
function sendMessage(e) {
  const message = document.getElementById("input-message-box").value;

  // If the user starts typing, switch focus to the input field
  if (document.getElementById("input-message-box") !== document.activeElement) {
    document.getElementById("input-message-box").focus();
  }

  if (e.keyCode === 13 && message !== "") {
    // Only send a message if they key is enter and the input field is not empty
    data = {
      channel: currentChannel,
      message: message
    };
    ipcRenderer.send("message", data);
    displayMessage(currentChannel, nick, data.message);
    document.getElementById("input-message-box").value = "";
  }

  // Scroll the message div to the bottom
  const messageDiv = document.querySelector(".message");
  messageDiv.scrollTop = messageDiv.scrollHeight;
}

/**
 * Takes a new message from the server and renders it on screen
 * @param {string} target The target channel
 * @param {string} user The user which sent the message
 * @param {string} message The message itself
 */
function displayMessage(target, user, message) {
  const timestamp = new Date().toLocaleTimeString();
  const html = `<div class="row"><div class="new-message"><span class="new-message-name">${user}<span class="new-message-time">${timestamp}</span><span class="new-message-message">${message}</span></div></div>`;
  const channel = document
    .getElementById(target)
    .getElementsByClassName("message")[0];
  channel.innerHTML += html;
}

/**
 * Handler for when joining a new channel. Adds all relevant HTML elements for rendering the new channel.
 * @param {string} channelName Name of channel joined
 */
function joinHandler(channelName) {
  if (document.getElementById(channelName) !== null) {
    return;
  }

  // Create a new div for the channel and add message containers
  const newDivHtml = `
                        <div class="message">
                            <div class="container-fluid">
                                &nbsp;
                            </div>
                        </div>`;

  const newDiv = document.createElement("div");
  newDiv.innerHTML = newDivHtml;
  newDiv.id = channelName;
  newDiv.style.display = "none";

  const beforeDiv = document.getElementById("input-message");
  document.getElementById("channel").insertBefore(newDiv, beforeDiv);

  // Create the new user list for the channel
  const newUserList = document.createElement("div");
  newUserList.className = channelName + "_List";
  newUserList.style.display = "none";
  document.getElementById("user-list").appendChild(newUserList);
}

/**
 * Handles switching channels when channel-list is clicked
 * @param {string} channelName The name of the channel switched to
 * @param {element} element The element clicked
 */
function switchChannel(channelName, element) {
  // Clear current channel
  if (currentChannel !== "") {
    document.getElementById(currentChannel).style.display = "none";
    document.getElementsByClassName(currentChannel + "_List")[0].style.display =
      "none";
  }

  // Remove all 'active' class names from the list
  channelElements = element.parentNode.childNodes;

  channelElements.forEach(channelElement => {
    channelElement.className = "list-group-item list-group-item-action";
  });

  // Add the 'active' class to the clicked element
  element.className =
    "list-group-item list-group-item-action list-group-item-secondary active";

  // Check to see if the user has already joined the channel
  const newChannel = document.getElementById(channelName);

  if (newChannel === null) {
    ipcRenderer.send("join-channel", channelName);
    joinHandler(channelName);
  }

  // Request a new user list
  ipcRenderer.send("request-user-list", channelName);

  // Change the channel title and topic
  document.getElementById("channel-title").innerHTML = channelName;
  // Need to retrieve the topic
  ipcRenderer.invoke("get-topic", channelName).then(result => {
    if (result === undefined) {
      document.getElementById("channel-topic").innerHTML = "";
    } else {
      document.getElementById("channel-topic").innerHTML = result;
    }
  });
  // Enable new channel
  document.getElementById(channelName).style.display = "block";
  document.getElementsByClassName(channelName + "_List")[0].style.display =
    "block";
  currentChannel = channelName;
}
